package id.java.hospital.services;

import java.util.List;

import id.java.hospital.models.db.Pegawai;
import id.java.hospital.models.request.EmployeeRequest;
import id.java.hospital.models.response.ResponseCombo;


public interface PegawaiService {

    List<Pegawai> findAll(int page, int size);
    Pegawai create(EmployeeRequest request);
    Pegawai findById(String id);
    void update(String email, EmployeeRequest request);
    ResponseCombo ComboQuery(String value);
    void SimpanImage(String filePath, String idUser);
    Pegawai findByEmail(String email);
    void Resset(String id, String password);
    
}
