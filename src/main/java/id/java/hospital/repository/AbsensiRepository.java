package id.java.hospital.repository;
import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.java.hospital.models.db.Absensi;

public interface AbsensiRepository extends JpaRepository<Absensi, Integer>{

    // find by today date and id
    @Query(value = "SELECT e FROM Absensi e WHERE e.tglAbsensi =:date and e.idUser.idUser =:id")
    Absensi findByCreationDate(@Param("date") LocalDate date, @Param("id") String id);
    
    // Range date and Id

    @Query(value = "SELECT e FROM Absensi e WHERE e.idUser.idUser =:id and e.tglAbsensi BETWEEN :datefrom and :dateto ")
    List<Absensi> findByDateandId(@Param("datefrom") LocalDate datefrom, @Param("dateto") LocalDate dateto, @Param("id") String id);


    // range date only
    @Query(value = "SELECT e FROM Absensi e WHERE e.tglAbsensi BETWEEN :datefrom and :dateto")
    List<Absensi> findByDateOnly(@Param("datefrom") LocalDate datefrom, @Param("dateto") LocalDate dateto);

    // Range date and status
    @Query(value = "SELECT e FROM Absensi e WHERE e.namaStatus<>'' and e.tglAbsensi BETWEEN :datefrom and :dateto")
    List<Absensi> findByStatus(@Param("datefrom") LocalDate datefrom, @Param("dateto") LocalDate dateto);

}
