# Employee Attendance 

Common API with Authorization  
JWT saved in cookies, no refresh Token 

## Build

Create Schema hospital   
Run `Mvn Spring-boot:run".   

## EndPoints

Employee

1. api/pegawai/combo/{value} 
2. api/pegawai/daftar 
3. api/pegawai/admin-tambah-pegawai 
4. api/pegawai/admin-ubah-pegawai (on testing)
5. api/pegawai/admin-ubah-photo (on testing)
6. api/pegawai/pegawai-ubah-photo (on testing)

Absensi

1. api/presensi/{InOut}
2. api/presensi/absensi 
3. api/presensi/daftar/admin 
3. api/presensi/daftar/pegawai 
4. api/presensi/combo/status-absen (on testing)

Authorization

1. api/auth/init-data
2. api/auth/login
3. api/auth/signout
4. api/auth/ubah-password-sendiri

## Result Test

Please find hospital.docx 
