package id.java.hospital.services;

import java.time.LocalDate;
import java.util.List;

import id.java.hospital.models.db.Absensi;
import id.java.hospital.models.request.AbsensiRequest;

public interface AbsensiService {

    List<Absensi> findAll(int page, int size);
    String create(String InOut, String email);
    void absen(AbsensiRequest request, String email);
    List<Absensi> DaftarPegawai(String email,LocalDate datefrom, LocalDate dateto);
    List<Absensi> DaftarAdmin(LocalDate datefrom, LocalDate dateto);
    List<Absensi> StatusAbsensi(LocalDate datefrom, LocalDate dateto);
    
}
