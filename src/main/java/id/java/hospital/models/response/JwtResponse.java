package id.java.hospital.models.response;

import id.java.hospital.models.db.Pegawai;

public class JwtResponse {
  private String token;
  private Pegawai pegawai;


  public JwtResponse(String accessToken, Pegawai pegawai) {
    this.token = accessToken;
    this.pegawai = pegawai;
   }


  public String getToken() {
    return token;
  }


  public void setToken(String token) {
    this.token = token;
  }


  public Pegawai getPegawai() {
    return pegawai;
  }


  public void setPegawai(Pegawai pegawai) {
    this.pegawai = pegawai;
  }



  
}
