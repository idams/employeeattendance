package id.java.hospital.models.db;

import java.time.LocalDate;
import java.util.UUID;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Table
@Entity
public class Absensi {


    @Id
    @Column(columnDefinition = "BINARY(16)")
    private UUID id = UUID.randomUUID();
    @ManyToOne
    @JoinColumn
    private Pegawai idUser;
    @Column
    private LocalDate tglAbsensi;
    @Column
    private long jamMasuk;
    @Column
    private long jamKeluar;
    @Column
    private String namaStatus;
    public UUID getId() {
        return id;
    }
    public void setId(UUID id) {
        this.id = id;
    }
    public Pegawai getIdUser() {
        return idUser;
    }
    public void setIdUser(Pegawai idUser) {
        this.idUser = idUser;
    }
    public long getJamMasuk() {
        return jamMasuk;
    }
    public void setJamMasuk(long jamMasuk) {
        this.jamMasuk = jamMasuk;
    }
    public long getJamKeluar() {
        return jamKeluar;
    }
    public void setJamKeluar(long jamKeluar) {
        this.jamKeluar = jamKeluar;
    }
    public String getNamaStatus() {
        return namaStatus;
    }
    public LocalDate getTglAbsensi() {
        return tglAbsensi;
    }
    public void setTglAbsensi(LocalDate tglAbsensi) {
        this.tglAbsensi = tglAbsensi;
    }
    public void setNamaStatus(String namaStatus) {
        this.namaStatus = namaStatus;
    }
        
}
