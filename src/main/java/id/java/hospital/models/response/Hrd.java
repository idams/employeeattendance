package id.java.hospital.models.response;

import lombok.Data;

@Data
public class Hrd {

    private String namaLengkap;
    private Integer KodeJabatan;
    private String namaJabatan;
 
}
