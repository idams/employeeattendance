package id.java.hospital.delivery;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import id.java.hospital.config.AppProperties;
import id.java.hospital.models.db.ERole;
import id.java.hospital.models.db.Pegawai;
import id.java.hospital.models.db.Role;
import id.java.hospital.models.db.User;
import id.java.hospital.models.request.AdminUbahFoto;
import id.java.hospital.models.request.EmployeeRequest;
import id.java.hospital.models.request.PegawaiUbahFoto;
import id.java.hospital.models.response.MessageResponse;
import id.java.hospital.models.response.ResponseCombo;
import id.java.hospital.repository.RoleRepository;
import id.java.hospital.repository.UserRepository;
import id.java.hospital.services.PegawaiService;
import id.java.hospital.utils.AbsensiUtils;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/pegawai")
public class PegawaiController {

    
    @Autowired
    PegawaiService pegawaiService;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    PasswordEncoder encoder;
    
    @Autowired
    AbsensiUtils absensiUtils;

    @Autowired
    AppProperties appProperties;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;
    //Query combo

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping(value = "/combo/{value}")
    public ResponseCombo ComboQuery(@PathVariable String value){
        return pegawaiService.ComboQuery(value);
    }

    @GetMapping(value = "/daftar", produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('ADMIN')")
    public List<Pegawai> findAll(@RequestParam(defaultValue = "0") int page,
                                    @RequestParam(defaultValue = "10") int size) {
        return pegawaiService.findAll(page, size); 
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping(value = "/admin-tambah-pegawai", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<?> create(@RequestBody EmployeeRequest request) {
        if(!request.getPassword().equals(null) && !(request.getPassword().equals(request.getPasswordC()))){
            return ResponseEntity.badRequest().body(appProperties.getWrongPass());
        }

        if (userRepository.existsByUsername(request.getNamalengkap())) {
            return ResponseEntity.badRequest().body(new MessageResponse(appProperties.getUserTaken()));
        }

        if (userRepository.existsByEmail(request.getEmail())) {
            return ResponseEntity.badRequest().body(new MessageResponse(appProperties.getEmailExist()));
        }
        
        // Create new user's account
        User user = new User(request.getNamalengkap(),
                            request.getEmail(),
                            encoder.encode(request.getPassword()));

        Set<Role> roles = new HashSet<>();

        Role modRole = roleRepository.findByName(ERole.ROLE_PEGAWAI)
              .orElseThrow(() -> new RuntimeException(appProperties.getRoleNotfound()));
        roles.add(modRole);
        user.setRoles(roles);

        Pegawai pgw = pegawaiService.create(request);
        user.setPegawai(pgw);
        userRepository.save(user);

        return ResponseEntity.ok().body(appProperties.getSuccess());
    }
    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping(value = "/admin-ubah-pegawai", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<?> update(@RequestBody EmployeeRequest request) {
        pegawaiService.update(request.getEmail(), request);
        return ResponseEntity.ok().body(appProperties.getSuccess());
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping(value = "/admin-ubah-photo", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<?> AdmUbahFoto(@PathVariable int id, @RequestBody AdminUbahFoto request) {

        pegawaiService.SimpanImage(request.getNamaFile(), request.getIdUser());
        return ResponseEntity.ok().body(appProperties.getSuccess());
    }
    @PreAuthorize("hasRole('PEGAWAI')")
    @PutMapping(value = "/pegawai-ubah-photo", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<?> PgwUbahFoto (@RequestBody PegawaiUbahFoto request) {

        pegawaiService.SimpanImage(request.getNamaFile(), absensiUtils.ConvertToId());
        return ResponseEntity.ok().body(appProperties.getSuccess());
    }
}
