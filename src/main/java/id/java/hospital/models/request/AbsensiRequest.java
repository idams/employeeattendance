package id.java.hospital.models.request;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class AbsensiRequest {


    @JsonProperty("Tanggal Absensi")
    private LocalDate tglAbsensi;
    @JsonProperty("Nama Status")
    private String namaStatus;

}
