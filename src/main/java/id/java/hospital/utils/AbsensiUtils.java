package id.java.hospital.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import id.java.hospital.models.db.User;
import id.java.hospital.repository.PegawaiRepository;
import id.java.hospital.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class AbsensiUtils {
    
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    PegawaiRepository pegawaiRepository;

    @Autowired
    UserRepository userRepository;

    //Get Id pegawai from Security

    public String ConvertToId(){
        UserDetails userDetails =(UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userRepository.findByUsername(userDetails.getUsername()).get();
        return user.getPegawai().getIdUser();
        //pegawaiRepository.findByEmail(userDetail.getEmail()).getIdUser();

    }


    public void ResetPassUser(String password){
        UserDetails userDetails =(UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        try {
            User user = userRepository.findByUsername(userDetails.getUsername()).get();
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		    user.setPassword(passwordEncoder.encode(password));
            userRepository.save(user);    
        } catch (Exception e) {
            log.info(e.getMessage());
        }
    }
    
    
}
