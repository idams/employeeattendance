package id.java.hospital.models.db;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Table(name="departemen")
@Entity
public class Departemen {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idDepartemen;
    @Column
    private String namaDepartemen;

    public Integer getIdDepartemen() {
        return idDepartemen;
    }
    public void setIdDepartemen(Integer idDepartemen) {
        this.idDepartemen = idDepartemen;
    }
    public String getNamaDepartemen() {
        return namaDepartemen;
    }
    public Departemen() {
    }
    public void setNamaDepartemen(String namaDepartemen) {
        this.namaDepartemen = namaDepartemen;
    }

    
    
}
