package id.java.hospital.delivery;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import id.java.hospital.config.AppProperties;
import id.java.hospital.models.db.Absensi;
import id.java.hospital.models.request.AbsensiRequest;
import id.java.hospital.models.request.StatusAbsen;
import id.java.hospital.services.AbsensiService;
import id.java.hospital.utils.AbsensiUtils;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/presensi")
public class AbsensiController {

    @Autowired
    private AbsensiService absensiService;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    AbsensiUtils absensiUtils;

    @Autowired
    AppProperties appProperties;

    //Data absensi
    @PreAuthorize("hasRole('ADMIN')") 
    @GetMapping(value = "/combo/status-absen", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Absensi> CheckInOut0(@RequestBody StatusAbsen absen) {
        // find by status is not empty
        return absensiService.StatusAbsensi(absen.getDatefrom(), absen.getDateto());
    }
    
    // Daftar Absensi Pegawai atau Admin
    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping(value = "/daftar/admin", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Absensi> findAllAdmin(@RequestBody StatusAbsen request) {

        // find by date or find by id
        return absensiService.DaftarAdmin(request.getDatefrom(), request.getDateto());                                 
    }

    @PreAuthorize("hasRole('PEGAWAI')")
    @GetMapping(value = "/daftar/pegawai", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Absensi> findAllPegawai(@RequestBody StatusAbsen request) {
        //find by id
        return absensiService.DaftarPegawai(absensiUtils.ConvertToId(), request.getDatefrom(), request.getDateto());                                 
    }

    //Check In Check Out
    @PreAuthorize("hasRole('PEGAWAI')")
    @GetMapping(value = "/{inout}", produces = MediaType.APPLICATION_JSON_VALUE)
    public String CheckInOut(@PathVariable String inout,@RequestParam(defaultValue = "0") int page,
                                    @RequestParam(defaultValue = "10") int size) {

        // In Out by email
        // save data jam masuk jam keluar
        return absensiService.create(inout, absensiUtils.ConvertToId());
    }

    // Absen tak Masuk
    @PreAuthorize("hasRole('PEGAWAI')")
    @PostMapping(value = "/absensi", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<?> Absen(@RequestBody AbsensiRequest request){

        absensiService.absen(request, absensiUtils.ConvertToId());
        return ResponseEntity.ok().body(appProperties.getSuccess());
    }

}
