package id.java.hospital.models.response;

import lombok.Data;

@Data
public class ResponseCombo<T> {

    private T Data;
    private String Description;
    
}
