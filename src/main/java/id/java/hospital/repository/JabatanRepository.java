package id.java.hospital.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import id.java.hospital.models.db.Jabatan;

public interface JabatanRepository extends JpaRepository<Jabatan, Integer>{
    
}
