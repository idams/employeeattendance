package id.java.hospital.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import id.java.hospital.models.db.JenisKelamin;

public interface JenisKelaminRepository extends JpaRepository<JenisKelamin, Integer>{
    
}
