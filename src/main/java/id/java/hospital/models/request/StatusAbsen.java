package id.java.hospital.models.request;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class StatusAbsen {
    
    @JsonProperty("Date from")
    //@JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate datefrom;
    @JsonProperty("Date to")
    //@JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateto;

}
