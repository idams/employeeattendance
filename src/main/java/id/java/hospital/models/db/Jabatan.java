package id.java.hospital.models.db;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Table
@Entity
public class Jabatan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idJabatan;
    @Column
    private String namaJabatan;

    public Integer getIdJabatan() {
        return idJabatan;
    }
    public void setIdJabatan(Integer idJabatan) {
        this.idJabatan = idJabatan;
    }
    public String getNamaJabatan() {
        return namaJabatan;
    }
    public void setNamaJabatan(String namaJabatan) {
        this.namaJabatan = namaJabatan;
    }
    public Jabatan() {
    }
    
}
