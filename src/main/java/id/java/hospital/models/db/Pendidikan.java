package id.java.hospital.models.db;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Table
@Entity
public class Pendidikan {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idPendidikan;
    @Column
    private String namaPendidikan;
    public Integer getIdPendidikan() {
        return idPendidikan;
    }
    public void setIdPendidikan(Integer idPendidikan) {
        this.idPendidikan = idPendidikan;
    }
    public String getNamaPendidikan() {
        return namaPendidikan;
    }
    public void setNamaPendidikan(String namaPendidikan) {
        this.namaPendidikan = namaPendidikan;
    }
    public Pendidikan() {
    }

    
}
