package id.java.hospital.services.impl;

import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import id.java.hospital.config.AppProperties;
import id.java.hospital.models.db.Absensi;
import id.java.hospital.models.db.Pegawai;
import id.java.hospital.models.request.AbsensiRequest;
import id.java.hospital.repository.AbsensiRepository;
import id.java.hospital.repository.PegawaiRepository;
import id.java.hospital.services.AbsensiService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AbsensiServiceImpl implements AbsensiService {

    @Autowired
    AbsensiRepository absensiRepository;

    @Autowired
    PegawaiRepository pegawaiRepository;

    @Autowired
    AppProperties appProperties;

    Instant instant = Instant.now();
    long timeStampSeconds = instant.getEpochSecond();
    LocalDate todayDate = LocalDate.now();


    @Override
    public List<Absensi> findAll(int page, int size) {

        PageRequest pageRequest = PageRequest.of(page, size);
        return absensiRepository.findAll(pageRequest)
                .getContent();
    }

    @Override
    public List<Absensi> StatusAbsensi(LocalDate datefrom, LocalDate dateto) {
        List<Absensi> list = new ArrayList<>();
        // find by status not empty and range date
        try {
            list = absensiRepository.findByStatus(datefrom, dateto);
        } catch (Exception e) {
            log.info(e.getMessage());
        } 
        return list;
    }

    @Override
    public List<Absensi> DaftarAdmin(LocalDate Datefrom, LocalDate Dateto) {
        List<Absensi> abs = new ArrayList<>();
        try{
            abs = absensiRepository.findByDateOnly(Datefrom, Dateto);
        }catch(Exception e){
            log.info(e.getMessage());
        }
        return abs;
    }

    @Override
    public List<Absensi> DaftarPegawai(String id, LocalDate Datefrom, LocalDate Dateto) {
        List<Absensi> abs = new ArrayList<>();
            // find by range date and id
        try{
            abs = absensiRepository.findByDateandId(Datefrom, Dateto, id);
        }catch(Exception e){
            log.info(e.getMessage());
        }
        return abs;
    }


    @Override
    public String create(String InOut, String id) {
        Pegawai pgw = pegawaiRepository.findById(id).get();
        
        if(InOut.equalsIgnoreCase(appProperties.getIn())){
            Absensi absensi = new Absensi();
            if(absensiRepository.findByCreationDate(todayDate, id) != null){
                return "Sudah Absen";
            }
            absensi.setIdUser(pgw);
            absensi.setJamMasuk(timeStampSeconds);
            absensi.setTglAbsensi(todayDate);
            absensiRepository.save(absensi);
        }else if(InOut.equalsIgnoreCase(appProperties.getOut())){
            //find by id per today
            Absensi abs = absensiRepository.findByCreationDate(todayDate, id);
            if(!abs.equals(null)){
                abs.setJamKeluar(timeStampSeconds);
                absensiRepository.save(abs);
            }else{
                Absensi absensi = new Absensi();
                absensi.setIdUser(pgw);
                absensi.setJamKeluar(timeStampSeconds);
                absensi.setTglAbsensi(todayDate);
                absensiRepository.save(absensi);
                
            }
                        
        }

        return "Ok";

    }

    @Override
    public void absen(AbsensiRequest request, String id) {
        Pegawai pgw = pegawaiRepository.findById(id).get();
        // find by today date if exist update data , else cek if today create new
        Absensi absen = absensiRepository.findByCreationDate(request.getTglAbsensi(), id);
        if(absen != null){
            absen.setNamaStatus(request.getNamaStatus());
            absensiRepository.save(absen);
        }else{
            Absensi absensi = new Absensi();
            absensi.setIdUser(pgw);
            absensi.setNamaStatus(request.getNamaStatus());
            absensi.setTglAbsensi(todayDate);
            absensiRepository.save(absensi);
        } 
    }
    
}
