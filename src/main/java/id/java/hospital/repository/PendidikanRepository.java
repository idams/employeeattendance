package id.java.hospital.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import id.java.hospital.models.db.Pendidikan;

public interface PendidikanRepository extends JpaRepository<Pendidikan, Integer> {
    
}
