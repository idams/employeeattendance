package id.java.hospital.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import id.java.hospital.models.db.Departemen;

public interface DepartemenRepository extends JpaRepository<Departemen, Integer>{
    
}
