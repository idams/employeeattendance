package id.java.hospital.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import id.java.hospital.models.db.UnitKerja;

public interface UnitKerjaRepository extends JpaRepository<UnitKerja, Integer>{
    
}
