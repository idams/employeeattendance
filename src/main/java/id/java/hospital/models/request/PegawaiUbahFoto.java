package id.java.hospital.models.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class PegawaiUbahFoto {
    @JsonProperty("Nama File")
    private String namaFile;
    @JsonProperty("Files")
    private String image;
    
}
