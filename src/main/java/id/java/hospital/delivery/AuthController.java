package id.java.hospital.delivery;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseCookie;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.java.hospital.config.AppProperties;
import id.java.hospital.models.db.ERole;
import id.java.hospital.models.db.Role;
import id.java.hospital.models.db.User;
import id.java.hospital.models.request.LoginRequest;
import id.java.hospital.models.request.SignupRequest;
import id.java.hospital.models.request.UbahPassword;
import id.java.hospital.models.response.MessageResponse;
import id.java.hospital.models.response.UserInfoResponse;
import id.java.hospital.repository.RoleRepository;
import id.java.hospital.repository.UserRepository;
import id.java.hospital.security.jwt.JwtUtils;
import id.java.hospital.security.services.UserDetailsImpl;
import id.java.hospital.services.PegawaiService;
import id.java.hospital.utils.AbsensiUtils;
import jakarta.validation.Valid;


@CrossOrigin(origins = "*", maxAge = 3600)
//for Angular Client (withCredentials)
//@CrossOrigin(origins = "http://localhost:8081", maxAge = 3600, allowCredentials="true")
@RestController
@RequestMapping("/api/auth")
public class AuthController {
  @Autowired
  AuthenticationManager authenticationManager;

  @Autowired
  UserRepository userRepository;

  @Autowired
  RoleRepository roleRepository;

  @Autowired
  PegawaiService pegawaiService;

  @Autowired
  AbsensiUtils absensiUtils;

  @Autowired
  PasswordEncoder encoder;

  @Autowired
  AppProperties appProperties;

  @Autowired
  JwtUtils jwtUtils;

  @PostMapping("/login")
  public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

    Authentication authentication = authenticationManager
        .authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

    SecurityContextHolder.getContext().setAuthentication(authentication);

    UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

    ResponseCookie jwtCookie = jwtUtils.generateJwtCookie(userDetails);

    List<String> roles = userDetails.getAuthorities().stream()
        .map(item -> item.getAuthority())
        .collect(Collectors.toList());
    if(roles.contains(appProperties.getPegawai())){
    User user = userRepository.findById(userDetails.getId()).get();
    return ResponseEntity.ok().header(HttpHeaders.SET_COOKIE, jwtCookie.toString())
        .body(user.getPegawai());
    }

    return ResponseEntity.ok().header(HttpHeaders.SET_COOKIE, jwtCookie.toString())
        .body(new UserInfoResponse(userDetails.getId(),
                                   userDetails.getUsername(),
                                   userDetails.getEmail(),
                                   roles));
  }

  @PostMapping("/init-data")
  public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
    if (userRepository.existsByUsername(signUpRequest.getUsername())) {
      return ResponseEntity.badRequest().body(new MessageResponse(appProperties.getUserTaken()));
    }

    if (userRepository.existsByEmail(signUpRequest.getEmail())) {
      return ResponseEntity.badRequest().body(new MessageResponse(appProperties.getEmailExist()));
    }

    // Create new user's account
    User user = new User(signUpRequest.getUsername(),
                         signUpRequest.getEmail(),
                         encoder.encode(signUpRequest.getPassword()));


    Set<Role> roles = new HashSet<>();
          Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
              .orElseThrow(() -> new RuntimeException(appProperties.getRoleNotfound()));
          roles.add(adminRole);    

    user.setRoles(roles);
    userRepository.save(user);

    return ResponseEntity.ok(new MessageResponse(appProperties.getSuccessReg()));
  }

  @PostMapping("/signout")
  public ResponseEntity<?> logoutUser() {
    ResponseCookie cookie = jwtUtils.getCleanJwtCookie();
    return ResponseEntity.ok().header(HttpHeaders.SET_COOKIE, cookie.toString())
        .body(new MessageResponse(appProperties.getSignOut()));
  }

  @PostMapping("/ubah-password-sendiri")
  @PreAuthorize("hasRole('PEGAWAI')")
  public ResponseEntity<?> UbahPasswordSendiri(@RequestBody UbahPassword ubah){
    
    pegawaiService.Resset(absensiUtils.ConvertToId(), ubah.getPassword1());
    absensiUtils.ResetPassUser(ubah.getPassword1());
    return ResponseEntity.ok().body(appProperties.getSuccess());
  }
}
