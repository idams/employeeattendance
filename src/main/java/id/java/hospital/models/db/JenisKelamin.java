package id.java.hospital.models.db;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Table
@Entity
public class JenisKelamin {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idKelamin;
    @Column
    private String namakelamin;

    public Integer getIdKelamin() {
        return idKelamin;
    }
    public void setIdKelamin(Integer idKelamin) {
        this.idKelamin = idKelamin;
    }
    public String getNamakelamin() {
        return namakelamin;
    }
    public void setNamakelamin(String namakelamin) {
        this.namakelamin = namakelamin;
    }
    public JenisKelamin(Integer idKelamin, String namakelamin) {
        this.idKelamin = idKelamin;
        this.namakelamin = namakelamin;
    }
    public JenisKelamin() {
          }
    


}
