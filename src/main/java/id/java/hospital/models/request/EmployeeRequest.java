package id.java.hospital.models.request;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class EmployeeRequest {

    @JsonProperty("Nama Lengkap")
    private String namalengkap;
    @JsonProperty("Email")
    private String email;
    @JsonProperty("Tempat Lahir")
    private String tempatLahir;
    @JsonProperty("Tanggal Lahir")
    private Integer tanggalLahir;
    @JsonProperty("Jenis Kelamin")
    private Integer kdJenisKelamin;
    @JsonProperty("Pendidikan")
    private Integer kdPendidikan;
    @JsonProperty("Jabatan")
    private Integer kdJabatan;
    @JsonProperty("Departemen")
    private Integer kdDepartemen;
    @JsonProperty("Unit Kerja")
    private Integer kdUnitKerja;
    @JsonProperty("Password")
    private String password;
    @JsonProperty("PasswordC")
    private String passwordC;
      
}