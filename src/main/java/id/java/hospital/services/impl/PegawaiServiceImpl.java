package id.java.hospital.services.impl;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.imageio.ImageIO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import id.java.hospital.config.AppProperties;
import id.java.hospital.models.db.Pegawai;
import id.java.hospital.models.request.EmployeeRequest;
import id.java.hospital.models.response.Hrd;
import id.java.hospital.models.response.ResponseCombo;
import id.java.hospital.repository.DepartemenRepository;
import id.java.hospital.repository.JabatanRepository;
import id.java.hospital.repository.JenisKelaminRepository;
import id.java.hospital.repository.PegawaiRepository;
import id.java.hospital.repository.PendidikanRepository;
import id.java.hospital.repository.UnitKerjaRepository;
import id.java.hospital.services.PegawaiService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PegawaiServiceImpl implements PegawaiService {

    
    @Autowired
    PegawaiRepository pegawaiRepository;

    @Autowired
    JenisKelaminRepository jenisKelaminRepository;

    @Autowired
    DepartemenRepository departemenRepository;

    @Autowired
    JabatanRepository jabatanRepository;

    @Autowired
    UnitKerjaRepository unitKerjaRepository;

    @Autowired
    PendidikanRepository pendidikanRepository;
    
    @Autowired
    AppProperties appProperties;

    //width and height image
    private int width = 963;
    private int height = 640;
    
    BufferedImage image = null;

    @Override
    public Pegawai findByEmail(String email){
        return pegawaiRepository.findByEmail(email);
    }

    public void Resset(String id, String password){
        try {

        Pegawai pgw = findById(id);
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		pgw.setPassword(passwordEncoder.encode(password));
            
        } catch (Exception e) {
            log.info(e.getMessage());
        }
        
        
    }
    
    @Override
    public Pegawai findById(String id) {
        return pegawaiRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @Override
    public List<Pegawai> findAll(int page, int size) {
        // page -> 0, can't minus
        // size -> 0, can't minus
        PageRequest pageRequest = PageRequest.of(page, size);
        return pegawaiRepository.findAll(pageRequest)
                .getContent();
    }

    @Override
    public void update(String email, EmployeeRequest request) {
        Pegawai pgw = findByEmail(email);
        String id = pgw.getIdUser();

        try {
        Pegawai pegawai = findById(id);
        pegawai.setNamaLengkap(request.getNamalengkap());
        pegawai.setEmail(request.getEmail());
        pegawai.setTempatLahir(request.getTempatLahir());
        pegawai.setTanggalLahir(request.getTanggalLahir());
        pegawai.setJenisKelamin(jenisKelaminRepository.findById(request.getKdJenisKelamin()).get());
        pegawai.setDepartemen(departemenRepository.findById(request.getKdDepartemen()).get());
        pegawai.setJabatan(jabatanRepository.findById(request.getKdJabatan()).get());
        pegawai.setUnitKerja(unitKerjaRepository.findById(request.getKdUnitKerja()).get());
        pegawai.setPendidikan(pendidikanRepository.findById(request.getKdPendidikan()).get());
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		pegawai.setPassword(passwordEncoder.encode(request.getPassword()));
        pegawaiRepository.save(pegawai);
            
        } catch (Exception e) {
            log.info(e.getMessage());
        }
    }


    @Override
    public Pegawai create(EmployeeRequest request) {
        Pegawai pegawai = new Pegawai();
        pegawai.setNamaLengkap(request.getNamalengkap());
        pegawai.setEmail(request.getEmail());
        pegawai.setTempatLahir(request.getTempatLahir());
        pegawai.setTanggalLahir(request.getTanggalLahir());
        pegawai.setJenisKelamin(jenisKelaminRepository.findById(request.getKdJenisKelamin()).get());
        pegawai.setDepartemen(departemenRepository.findById(request.getKdDepartemen()).get());
        pegawai.setJabatan(jabatanRepository.findById(request.getKdJabatan()).get());
        pegawai.setUnitKerja(unitKerjaRepository.findById(request.getKdUnitKerja()).get());
        pegawai.setPendidikan(pendidikanRepository.findById(request.getKdPendidikan()).get());
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		pegawai.setPassword(passwordEncoder.encode(request.getPassword()));
        pegawaiRepository.save(pegawai);
        return pegawai;
    }


    public ResponseCombo ComboQuery(String value){

        ResponseCombo combo = new ResponseCombo();
        if(value.equalsIgnoreCase(appProperties.getJabatan())){
            combo.setData(jabatanRepository.findAll());
        }else if(value.equalsIgnoreCase(appProperties.getDepartemen())){
            combo.setData(departemenRepository.findAll());
        }else if(value.equalsIgnoreCase(appProperties.getJeniskelamin())){
            combo.setData(jenisKelaminRepository.findAll());
        }else if(value.equalsIgnoreCase(appProperties.getPendidikan())){
            combo.setData(pendidikanRepository.findAll());
        }else if(value.equalsIgnoreCase(appProperties.getUnitKerja())){
            combo.setData(unitKerjaRepository.findAll());
        }else if(value.equalsIgnoreCase(appProperties.getDepartemenhrd())){

            try {
                
            List<Pegawai> pgw = pegawaiRepository.findHrd();
            Hrd hrd = new Hrd();
            List<Hrd> List = new ArrayList<>();  
            for(int i=0; i<pgw.size(); i++){
                hrd.setNamaLengkap(pgw.get(i).getNamaLengkap());
                hrd.setKodeJabatan(pgw.get(i).getJabatan().getIdJabatan());
                hrd.setNamaJabatan(pgw.get(i).getJabatan().getNamaJabatan());
                List.add(hrd);
            }
            combo.setData(List);
               
            } catch (Exception e) {
                log.info(e.getMessage());
            }
            
            
        }
        return combo;
    }


    public void SimpanImage(String filePath, String idUser){
        try {
            File input_file = new File(filePath);
            image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
  
            // Reading input file
            image = ImageIO.read(input_file);
        } catch (Exception e) {
            log.info(e.getMessage());
        }

        final ByteArrayOutputStream os = new ByteArrayOutputStream();
        try (final OutputStream b64os = Base64.getEncoder().wrap(os)) {
            ImageIO.write(image, "JPEG", b64os);
        } catch (Exception e) {
            log.info(e.getMessage());
        }   
        
        // Save to Pegawai
        Pegawai pegawai = findById(idUser);
        pegawai.setPhoto(os.toString());
    }
    
    
}
