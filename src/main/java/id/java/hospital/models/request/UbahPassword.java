package id.java.hospital.models.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class UbahPassword {

    @JsonProperty("PasswordAsli")
    private String password;
    @JsonProperty("PasswordBaru1")
    private String password1;
    @JsonProperty("PasswordBaru2")
    private String password2;



    
}
