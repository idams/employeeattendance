package id.java.hospital.models.db;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table
public class UnitKerja {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idUnitKerja;
    private String namaUnitKerja;
    public Integer getIdUnitKerja() {
        return idUnitKerja;
    }
    public void setIdUnitKerja(Integer idUnitKerja) {
        this.idUnitKerja = idUnitKerja;
    }
    public String getNamaUnitKerja() {
        return namaUnitKerja;
    }
    public void setNamaUnitKerja(String namaUnitKerja) {
        this.namaUnitKerja = namaUnitKerja;
    }

    
}
