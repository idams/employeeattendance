package id.java.hospital.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import id.java.hospital.models.db.Pegawai;

public interface PegawaiRepository extends JpaRepository<Pegawai, String>{

    
    @Query(value="SELECT p from Pegawai p inner join jabatan j on j.idJabatan = p.jabatan.idJabatan where j.idJabatan=1")
    List<Pegawai> findHrd();

    Pegawai findByEmail(String email);
    
    
}
