package id.java.hospital.models.db;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Table
@Entity
public class Pegawai {
    
    @Id
    @GeneratedValue(strategy=GenerationType.UUID)
    private String idUser;
    @Column
    private String profile;
    @Column
    private String namaLengkap;
    @Column
    private String tempatLahir;
    @Column
    private Integer tanggalLahir;
    @Column
    private String email;
    @Column
    private String password;
    @Column
    private String nikUser;
    @ManyToOne
    @JoinColumn(referencedColumnName = "idJabatan")
    private Jabatan jabatan;
    @ManyToOne
    @JoinColumn(referencedColumnName = "idDepartemen")
    private Departemen departemen;
    @ManyToOne
    @JoinColumn(referencedColumnName = "idUnitKerja")
    private UnitKerja unitKerja;
    @ManyToOne
    @JoinColumn(referencedColumnName = "idKelamin")
    private JenisKelamin jenisKelamin;
    @ManyToOne
    @JoinColumn(referencedColumnName = "idPendidikan")
    private Pendidikan pendidikan;
    @Column
    private String photo;

    public JenisKelamin getJenisKelamin() {
        return jenisKelamin;
    }
    public void setJenisKelamin(JenisKelamin jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }
    public String getPhoto() {
        return photo;
    }
    public void setPhoto(String photo) {
        this.photo = photo;
    }
    
    
    
    public String getProfile() {
        return profile;
    }
    public void setProfile(String profile) {
        this.profile = profile;
    }
    public String getNamaLengkap() {
        return namaLengkap;
    }
    public void setNamaLengkap(String namaLengkap) {
        this.namaLengkap = namaLengkap;
    }
    public String getTempatLahir() {
        return tempatLahir;
    }
    public void setTempatLahir(String tempatLahir) {
        this.tempatLahir = tempatLahir;
    }
    public Integer getTanggalLahir() {
        return tanggalLahir;
    }
    public void setTanggalLahir(Integer tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public Jabatan getJabatan() {
        return jabatan;
    }
    public void setJabatan(Jabatan jabatan) {
        this.jabatan = jabatan;
    }
    public Departemen getDepartemen() {
        return departemen;
    }
    public void setDepartemen(Departemen departemen) {
        this.departemen = departemen;
    }
    public UnitKerja getUnitKerja() {
        return unitKerja;
    }
    public void setUnitKerja(UnitKerja unitKerja) {
        this.unitKerja = unitKerja;
    }
    public Pendidikan getPendidikan() {
        return pendidikan;
    }
    public void setPendidikan(Pendidikan pendidikan) {
        this.pendidikan = pendidikan;
    }
    public String getNikUser() {
        return nikUser;
    }
    public void setNikUser(String nikUser) {
        this.nikUser = nikUser;
    }
    public String getIdUser() {
        return idUser;
    }
    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }
        
}
